package com.application.projet_pirates;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

public class SettingsActivity extends Activity {
	
	SoundPool sp;
	SharedPreferences sharedPref;
	int soundId;
	static boolean isCheckedSound;
	
	int activeColor, unactiveColor;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// remove title
		
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.settings_activity);
		sharedPref = SettingsPrefs
				.getSharedPreferences(getApplicationContext());

		// prepare Audio file
		sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		soundId = sp.load(getApplicationContext(), R.raw.clic, 1);

		Typeface font;
		// Retrieve each Button
		TextView zoneType = (TextView) findViewById(R.id.zoneType);
		TextView soundState = (TextView) findViewById(R.id.soundState);
		RadioButton static_zone = (RadioButton) findViewById(R.id.static_zone);
		RadioButton dynamic_zone = (RadioButton) findViewById(R.id.dynamic_zone);
		RadioButton soundOn = (RadioButton) findViewById(R.id.soundOn);
		RadioButton soundOff = (RadioButton) findViewById(R.id.soundOff);

		// use custom fonts and apply to Button
		font = Typeface.createFromAsset(getAssets(), "jacinto.otf");
		zoneType.setTypeface(font);
		static_zone.setTypeface(font);
		dynamic_zone.setTypeface(font);
		soundState.setTypeface(font);
		soundOn.setTypeface(font);
		soundOff.setTypeface(font);
		zoneType.setPaintFlags(zoneType.getPaintFlags()
				| Paint.UNDERLINE_TEXT_FLAG);
		soundState.setPaintFlags(soundState.getPaintFlags()
				| Paint.UNDERLINE_TEXT_FLAG);

		activeColor = Color.parseColor("#cc3333");
		unactiveColor = Color.parseColor("#FFFFFF");
		// TODO sharedPreferences
		boolean defaultZone = false;
		boolean isActivatedZone = sharedPref.getBoolean(
				getString(R.string.dynamic_zone), defaultZone);
		static_zone.setActivated(!isActivatedZone);
		dynamic_zone.setActivated(isActivatedZone);
		if (isActivatedZone)
			dynamic_zone.setTextColor(activeColor);
		else
			static_zone.setTextColor(activeColor);

		boolean defaultSoundState = true;
		isCheckedSound = SettingsPrefs.getSoundState(getApplicationContext());
		if (isCheckedSound)
			soundOn.setTextColor(activeColor);
		else
			soundOff.setTextColor(activeColor);

		Log.v("Sound State", isCheckedSound + "");
		OnClickListener menuClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				SharedPreferences.Editor editor = sharedPref.edit();
				switch (v.getId()) {
				case R.id.dynamic_zone:
					((RadioButton) v).setTextColor(activeColor);
					((RadioButton) findViewById(R.id.static_zone))
							.setTextColor(unactiveColor);
					// TODO sharedPreferences
					editor.putBoolean(getString(R.string.dynamic_zone), true);
					editor.putBoolean(getString(R.string.static_zone), false);
					break;

				case R.id.static_zone:
					((RadioButton) findViewById(R.id.dynamic_zone))
							.setTextColor(unactiveColor);
					((RadioButton) v).setTextColor(activeColor);
					// TODO sharedPreferences
					editor.putBoolean(getString(R.string.dynamic_zone), false);
					editor.putBoolean(getString(R.string.static_zone), true);
					break;

				case R.id.soundOn:
					isCheckedSound=true;
					((RadioButton) findViewById(R.id.soundOff))
							.setTextColor(unactiveColor);
					((RadioButton) v).setTextColor(activeColor);
					// TODO sharedPreferences
					editor.putBoolean(getString(R.string.soundState), true);
					break;
				case R.id.soundOff:
					isCheckedSound=false;
					((RadioButton) findViewById(R.id.soundOn))
							.setTextColor(unactiveColor);
					((RadioButton) v).setTextColor(activeColor);
					// TODO sharedPreferences
					editor.putBoolean(getString(R.string.soundState), false);
					break;

				}
				if (isCheckedSound)
					sp.play(soundId, 1, 1, 0, 0, 1);// ringtone when click
				editor.commit();

			}

		};

		// bind Buttons to the listener
		dynamic_zone.setOnClickListener(menuClickListener);
		static_zone.setOnClickListener(menuClickListener);
		soundOn.setOnClickListener(menuClickListener);
		soundOff.setOnClickListener(menuClickListener);

	}

	public boolean getSoundState() {
		return SettingsPrefs.getSoundState(getApplicationContext());
		// return getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE).getBoolean(
		// getString(R.string.soundState),true);
	}

	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(0, 0);
	}

}