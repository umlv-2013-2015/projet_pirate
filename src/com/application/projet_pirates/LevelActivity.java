package com.application.projet_pirates;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class LevelActivity extends Activity {
	SoundPool sp;
	int soundId;// creer un tableau d'int pour charger plusieurs sons
	// differents.
	Typeface font;
	// private Intent gameIntent ;
	private MediaPlayer menuMusic;
	boolean sound;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		sound = getSoundState();
		Log.v("soundStateMenu", sound + "");
		// remove title, fullscreen mode
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.level_activity);

		// Retrieve each Button
		TextView choose_level = (TextView) findViewById(R.id.choose_level);
		Button lvl1 = (Button) findViewById(R.id.lvl1);
		Button lvl2 = (Button) findViewById(R.id.lvl2);
		Button lvl3 = (Button) findViewById(R.id.lvl3);
		Button lvl4 = (Button) findViewById(R.id.lvl4);
		Button lvl5 = (Button) findViewById(R.id.lvl5);

		// use custom fonts and apply to Button
		font = Typeface.createFromAsset(getAssets(), "jacinto.otf");
		choose_level.setTypeface(font);
		lvl1.setTypeface(font);
		lvl2.setTypeface(font);
		lvl3.setTypeface(font);
		lvl4.setTypeface(font);
		lvl5.setTypeface(font);
		// prepare Audio file
		sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		// load ringtone to get its Id.
		soundId = sp.load(getApplicationContext(), R.raw.clic, 1);
		// menu song
		menuMusic = MediaPlayer.create(getApplicationContext(),
				R.raw.menu_music);
		menuMusic.setLooping(true);
		menuMusic.setAudioStreamType(AudioManager.STREAM_MUSIC);
		if (sound)
			menuMusic.start();
		// OnClickListener : Check if we touch any button
		OnClickListener levelClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				default:
					// String value = ((TextView) v).getText().toString();
					Intent gameIntent = new Intent(LevelActivity.this,
							GameActivity.class);
					gameIntent.putExtra("level", Integer
							.parseInt(   ((TextView) v).getText().toString().split(" ")[1])    );
					System.out.println(Integer
					.parseInt(   ((TextView) v).getText().toString().split(" ")[1])) ;
					startActivity(gameIntent);
					break;
				}
				if (getSoundState())
					sp.play(soundId, 1, 1, 0, 0, 1);// ringtone when click
			}

		};

		// bind Buttons to the listener
		 lvl1.setOnClickListener(levelClickListener);
		 lvl2.setOnClickListener(levelClickListener);
		 lvl3.setOnClickListener(levelClickListener);
		 lvl4.setOnClickListener(levelClickListener);
		 lvl5.setOnClickListener(levelClickListener);

	}

	@Override
	protected void onPause() {
		super.onPause();
		if (getSoundState()) {
			menuMusic.pause();
			menuMusic.seekTo(0);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (getSoundState()) {
			menuMusic.start();
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (getSoundState())
			menuMusic.stop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public boolean getSoundState() {
		return getSharedPreferences(SettingsPrefs.PREF_FILE_NAME, MODE_PRIVATE)
				.getBoolean(getString(R.string.soundState), true);
	}

}
