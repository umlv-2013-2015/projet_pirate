//package com.application.projet_pirates;
//
//import java.util.ArrayList;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.FragmentTransaction;
//import android.support.v4.app.ListFragment;
//import android.util.Log;
//import android.view.View;
//import android.widget.ArrayAdapter;
//import android.widget.ListAdapter;
//import android.widget.ListView;
//import android.widget.Toast;
//
//public class LvlListFragment extends ListFragment {
//	private boolean mDualPane;
//	
//	public LvlListFragment() {
//		// Call on reconstruction : no args in there
//	}
//
//	public static LvlListFragment create(ArrayList<String> list) {
//		LvlListFragment fragment = new LvlListFragment();
//		Bundle arguments = new Bundle();
//		
//		arguments.putStringArrayList("urls", list);
//		fragment.setArguments(arguments);
//		
//		return fragment;
//	}
//
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		
//		ArrayList<String> list = new ArrayList<String>();
//		list.add("level 1");
//		list.add("level 2");
//		list.add("level 3");
//		list.add("level 4");
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1, list);
//		setListAdapter(adapter);
//		
//		Log.w("App", "LIST State >> " + list==null?"NULL":"NOT NULL");
//	}
//	
//	@Override
//	public void onActivityCreated(Bundle savedInstanceState) {
//		super.onActivityCreated(savedInstanceState);
//		
//		// Check to see if we have a frame in which to embed the details
//        // fragment directly in the containing UI.
//        View detailsFrame = getActivity().findViewById(R.id.details);
//        mDualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;
//		
//		if (mDualPane) {
//			// In dual-pane mode, the list view highlights the selected item.
//            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
//            Log.w("App", "Screen into Dual Pane mode.");
//		}
//	}
//
//	@Override
//	public void onListItemClick(ListView l, View v, int position, long id) {
//		super.onListItemClick(l, v, position, id);
//		
//		Toast.makeText(getActivity(), "Item " + position + " was clicked", Toast.LENGTH_SHORT).show();
//		String clickedDetail = (String)l.getItemAtPosition(position);
//		Log.w("App", "URL [" + clickedDetail + "]");
//		
//		showDetails(position, clickedDetail);
//	}
//
//	private void showDetails(int index, String clickedDetail) {
//		if (mDualPane) {
//			// We can display everything in-place with fragments, so update
//			// the list to highlight the selected item and show the data.
//			getListView().setItemChecked(index, true);
//
//			// Check what fragment is currently shown, replace if needed.
//			WebPageFragment details = (WebPageFragment)getFragmentManager().findFragmentById(R.id.details);
//			if (details == null || !details.getShownUrl().equals(clickedDetail)) {
//				// Make new fragment to show this selection.
//				details = WebPageFragment.create(clickedDetail);
//				
//				FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//
//				fragmentTransaction.replace(R.id.details, details);
//				fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
//				fragmentTransaction.commit();
//			}
//		} else {
//			// Otherwise we need to launch a new activity to display
//			// the dialog fragment with selected text.
//			Intent i = new Intent(getActivity(), WebPageActivity.class);
//			i.putExtra("link", clickedDetail);
//			startActivity(i);
//		}
//	}
//}
