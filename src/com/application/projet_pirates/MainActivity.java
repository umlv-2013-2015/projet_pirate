package com.application.projet_pirates;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	SoundPool sp;
	int soundId;// creer un tableau d'int pour charger plusieurs sons
	// differents.
	Typeface font;
	// private Intent gameIntent ;
	private MediaPlayer menuMusic;
	boolean sound;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		sound = getSoundState();
		Log.v("soundStateMenu", sound + "");
		// remove title, fullscreen mode
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_main);

		// Retrieve each Button
		TextView app_title = (TextView) findViewById(R.id.app_title);
		Button play = (Button) findViewById(R.id.play);
		Button settings = (Button) findViewById(R.id.settings);
		Button about = (Button) findViewById(R.id.about);
		Button exit = (Button) findViewById(R.id.exit);

		// use custom fonts and apply to Button
		font = Typeface.createFromAsset(getAssets(), "jacinto.otf");
		app_title.setTypeface(font);
		play.setTypeface(font);
		settings.setTypeface(font);
		about.setTypeface(font);
		exit.setTypeface(font);

		// prepare Audio file
		sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
		// load ringtone to get its Id.
		soundId = sp.load(getApplicationContext(), R.raw.clic, 1);
		// menu song
		menuMusic = MediaPlayer.create(getApplicationContext(),
				R.raw.menu_music);
		menuMusic.setLooping(true);
		menuMusic.setAudioStreamType(AudioManager.STREAM_MUSIC);
		if (sound)
			menuMusic.start();
		// OnClickListener : Check if we touch any button
		OnClickListener menuClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.play:
					Intent levelIntent = new Intent(MainActivity.this,
							LevelActivity.class);
					startActivity(levelIntent);
					break;
				case R.id.settings:
					Intent settingsIntent = new Intent(MainActivity.this,
							SettingsActivity.class);
					settingsIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					startActivity(settingsIntent);
					break;
				case R.id.about:
					Intent aboutIntent = new Intent(MainActivity.this,
							AboutActivity.class);
					aboutIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					startActivity(aboutIntent);
					break;
				case R.id.exit:
					finish();
					break;
				}
				if (getSoundState())
					sp.play(soundId, 1, 1, 0, 0, 1);// ringtone when click
			}

		};

		// bind Buttons to the listener
		play.setOnClickListener(menuClickListener);
		settings.setOnClickListener(menuClickListener);
		about.setOnClickListener(menuClickListener);
		exit.setOnClickListener(menuClickListener);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (getSoundState()) {
			menuMusic.pause();
			menuMusic.seekTo(0);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (getSoundState()) {
			menuMusic.start();
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (getSoundState())
			menuMusic.stop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public boolean getSoundState() {
		return getSharedPreferences(SettingsPrefs.PREF_FILE_NAME, MODE_PRIVATE)
				.getBoolean(getString(R.string.soundState), true);
	}

}
