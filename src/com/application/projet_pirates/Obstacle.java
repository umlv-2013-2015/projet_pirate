package com.application.projet_pirates;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class Obstacle implements Crossable {

	protected Point p1;
	protected Point p2;

	protected SurfacePanel level;

	/*
	 * Create a new obstacle (shape of a line) from 2 points, the 1st point must
	 * be on the left or on top of the 2nd.
	 * 
	 * @param level the SurfacePanel containing this obstacle
	 * 
	 * @param x1, y1 coord of the 1st point
	 * 
	 * @param x2, y2 coord of the 2nd point
	 */
	public Obstacle(SurfacePanel level, int x1, int y1, int x2, int y2) {
		this.p1 = new Point(x1, y1);
		this.p2 = new Point(x2, y2);

		this.level = level;
	}

	// draw dots from pos to pos+hei/wid
	@Override
	public void draw(Canvas canvas, int size, int drawFromHeight,
			int drawFromWidth) {
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setStyle(Paint.Style.FILL);
		canvas.drawRect(drawFromWidth + (p1.x * size), drawFromHeight
				+ (p1.y * size), drawFromWidth + ((p2.x + 10) * size),
				drawFromHeight + ((p2.y + 10) * size), paint);

	}

	public boolean isAlive() {
		return false;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		builder.append(p1.x);
		builder.append(",");
		builder.append(p1.y);
		builder.append(",");
		builder.append(p2.x);
		builder.append(",");
		builder.append(p2.y);
		builder.append("]");
		return builder.toString();
	}
}
