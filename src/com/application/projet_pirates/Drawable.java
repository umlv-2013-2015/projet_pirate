package com.application.projet_pirates;

import android.graphics.Canvas;

public interface Drawable {
	public void draw(Canvas canvas, int size, int drawFromHeight, int drawFromWidth);
}
