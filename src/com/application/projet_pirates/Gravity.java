package com.application.projet_pirates;

import android.util.Log;

public enum Gravity {
	TOP {
		public int getSpeed(Point movement) {
			return (int) Math.abs(movement.x);
		}

		public void apply(Point pos) {
			pos.y--;
		}

		@Override
		public void turnAround(Point movement) {
			if (movement.x > 0)
				movement.x = -1;
			else
				movement.x = 1;
		}

		@Override
		public boolean checkGround(SurfacePanel level, Pirate pirate) {
			boolean readyToAccel = pirate.onAir;
			if (!checkPos(level, new Point(pirate.position.x,
					pirate.position.y - 1))) {
				this.apply(pirate.movement);
				return false;
			} else {
				if (readyToAccel)
					pirate.accel = 8;

			}
			pirate.movement.y = 0;
			return true;
		}

		@Override
		public void jump(Pirate pirate) {
			pirate.movement.y += (JUMP + pirate.getSpeed());

		}

		@Override
		public int getAnimationRow(Point movement) {
			if (movement.x < 0)
				return 6;
			return 7;
		}

		@Override
		public void setMovementAndGravityFromObs(Pirate pirate, Obstacle obs) {
			int tmpX = pirate.movement.x;
			int tmpY = pirate.movement.y;
			int dir = 1;
			if (!defineFromCorner(pirate, obs)) {
				if (pirate.position.x > (obs.p1.x - 10)
						&& pirate.position.x < (obs.p2.x + 10)) {

					pirate.movement.y = 0;

					if (tmpY < 0)
						pirate.gravity = TOP;
					else
						pirate.gravity = BOTTOM;

				} else {
					if (tmpX < 0)
						pirate.gravity = LEFT;
					else
						pirate.gravity = RIGHT;
					if (tmpY < 0)
						dir = -1;
					pirate.movement.y = Math.abs(tmpX) * dir;
					pirate.movement.x = 0;
				}
			}
		}

		@Override
		protected void setFromCorner(Pirate pirate, int i) {

			switch (i) {
			case 1:
				pirate.movement.y = -pirate.getSpeed();
				pirate.movement.x = -1;
				pirate.gravity = RIGHT;
				break;
			case 3:
				pirate.movement.y = -pirate.getSpeed();
				pirate.movement.x = +1;
				pirate.gravity = LEFT;
				break;
			default:
				Log.e("setfromcorner", "ERROR");
			}
		}

		@Override
		public void speedUp(Point movement) {
			if (movement.x > 0)
				movement.x++;
			else
				movement.x--;
		}
	},
	RIGHT {
		public int getSpeed(Point movement) {
			return (int) Math.abs(movement.y);
		}

		public void apply(Point pos) {
			pos.x++;
		}

		@Override
		public void turnAround(Point movement) {
			if (movement.y > 0)
				movement.y = -1;
			else
				movement.y = 1;

		}

		@Override
		public boolean checkGround(SurfacePanel level, Pirate pirate) {
			boolean readyToAccel = pirate.onAir;
			if (!checkPos(level, new Point(pirate.position.x + 1,
					pirate.position.y))) {
				this.apply(pirate.movement);
				return false;
			} else {
				if (readyToAccel)
					pirate.accel = 3;
			}
			pirate.movement.x = 0;
			return true;
		}

		@Override
		public void jump(Pirate pirate) {
			pirate.movement.x -= (JUMP + pirate.getSpeed());

		}

		@Override
		public int getAnimationRow(Point movement) {
			if (movement.y < 0)
				return 4;
			return 5;
		}

		@Override
		public void setMovementAndGravityFromObs(Pirate pirate, Obstacle obs) {
			int tmpX = pirate.movement.x;
			int tmpY = pirate.movement.y;
			int dir = 1;
			if (!defineFromCorner(pirate, obs)) {
				if (pirate.position.y > (obs.p1.y - 10)
						&& pirate.position.y < (obs.p2.y + 10)) {
					pirate.movement.x = 0;
					if (tmpX < 0)
						pirate.gravity = LEFT;
					else
						pirate.gravity = RIGHT;

				} else {
					if (tmpY < 0)
						pirate.gravity = TOP;
					else
						pirate.gravity = BOTTOM;

					if (pirate.movement.x < 0)
						dir = -1;
					pirate.movement.x = Math.abs(pirate.movement.y) * dir;
					pirate.movement.y = 0;
				}
			}
		}

		@Override
		protected void setFromCorner(Pirate pirate, int i) {

			switch (i) {
			case 0:
				pirate.movement.x = pirate.getSpeed();
				pirate.movement.y = -1;
				pirate.gravity = BOTTOM;
				break;
			case 1:
				pirate.movement.x = pirate.getSpeed();
				pirate.movement.y = +1;
				pirate.gravity = TOP;
				break;
			default:
				Log.e("setfromcorner", "ERROR");
			}

		}

		@Override
		public void speedUp(Point movement) {
			if (movement.y > 0)
				movement.y++;
			else
				movement.y--;

		}
	},
	BOTTOM {
		public int getSpeed(Point movement) {
			return (int) Math.abs(movement.x);
		}

		public void apply(Point pos) {
			pos.y++;
		}

		@Override
		public void turnAround(Point movement) {
			// Log.e("BOTTOM", "TURN AROUND");
			if (movement.x > 0) {
				movement.x = -1;
			} else {
				movement.x = 1;
			}
		}

		@Override
		public boolean checkGround(SurfacePanel level, Pirate pirate) {
			boolean readyToAccel = pirate.onAir;
			if (!checkPos(level, new Point(pirate.position.x,
					pirate.position.y + 1))) {
				// Log.w("BOTTOM", "ground NOK");
				this.apply(pirate.movement);
				return false;
			} else {
				if (readyToAccel)
					pirate.accel = 3;
			}
			pirate.movement.y = 0;
			return true;
		}

		@Override
		public void jump(Pirate pirate) {
			pirate.movement.y -= (JUMP + pirate.getSpeed());

		}

		@Override
		public int getAnimationRow(Point movement) {
			if (movement.x < 0)
				return 0;
			return 1;
		}

		@Override
		public void setMovementAndGravityFromObs(Pirate pirate, Obstacle obs) {
			int tmpX = pirate.movement.x;
			int tmpY = pirate.movement.y;
			int dir = 1;
			if (!defineFromCorner(pirate, obs)) {
				if (pirate.position.x > (obs.p1.x - 10)
						&& pirate.position.x < (obs.p2.x + 10)) {

					pirate.movement.y = 0;

					if (tmpY < 0)
						pirate.gravity = TOP;
					else
						pirate.gravity = BOTTOM;

				} else {
					if (tmpX < 0)
						pirate.gravity = LEFT;
					else
						pirate.gravity = RIGHT;
					if (tmpY < 0)
						dir = -1;
					pirate.movement.y = Math.abs(tmpX) * dir;
					pirate.movement.x = 0;
				}
			}
		}

		@Override
		protected void setFromCorner(Pirate pirate, int i) {

			switch (i) {
			case 0:
				pirate.movement.y = pirate.getSpeed();
				pirate.movement.x = -1;
				pirate.gravity = RIGHT;
				break;
			case 2:
				pirate.movement.y = pirate.getSpeed();
				pirate.movement.x = +1;
				pirate.gravity = LEFT;
				break;
			}

		}

		@Override
		public void speedUp(Point movement) {
			if (movement.x > 0)
				movement.x++;
			else
				movement.x--;
		}
	},
	LEFT {
		public int getSpeed(Point movement) {
			return (int) Math.abs(movement.y);
		}

		public void apply(Point pos) {
			pos.x--;
		}

		@Override
		public void turnAround(Point movement) {
			if (movement.y > 0)
				movement.y = -1;
			else
				movement.y = 1;
		}

		@Override
		public boolean checkGround(SurfacePanel level, Pirate pirate) {
			boolean readyToAccel = pirate.onAir;
			if (!checkPos(level, new Point(pirate.position.x - 1,
					pirate.position.y))) {
				this.apply(pirate.movement);
				return false;
			} else {
				if (readyToAccel)
					pirate.accel = 3;
			}
			pirate.movement.x = 0;
			return true;
		}

		@Override
		public void jump(Pirate pirate) {
			pirate.movement.x += (JUMP + pirate.getSpeed());

		}

		@Override
		public int getAnimationRow(Point movement) {
			if (movement.y < 0)
				return 2;
			return 3;
		}

		@Override
		public void setMovementAndGravityFromObs(Pirate pirate, Obstacle obs) {
			int tmpX = pirate.movement.x;
			int tmpY = pirate.movement.y;
			int dir = 1;
			if (!defineFromCorner(pirate, obs)) {
				if (pirate.position.y > (obs.p1.y - 10)
						&& pirate.position.y < (obs.p2.y + 10)) {
					pirate.movement.x = 0;
					if (tmpX < 0)
						pirate.gravity = LEFT;
					else
						pirate.gravity = RIGHT;

				} else {
					if (tmpY < 0)
						pirate.gravity = TOP;
					else
						pirate.gravity = BOTTOM;

					if (pirate.movement.x < 0)
						dir = -1;
					pirate.movement.x = Math.abs(pirate.movement.y) * dir;
					pirate.movement.y = 0;
				}
			}
		}

		@Override
		protected void setFromCorner(Pirate pirate, int i) {

			switch (i) {
			case 2:
				pirate.movement.x = -pirate.getSpeed();
				pirate.movement.y = -1;
				pirate.gravity = BOTTOM;
				break;
			case 3:
				pirate.movement.x = -pirate.getSpeed();
				pirate.movement.y = +1;
				pirate.gravity = TOP;
				break;
			}
		}

		@Override
		public void speedUp(Point movement) {
			if (movement.y > 0)
				movement.y++;
			else
				movement.y--;
		}
	};

	private static boolean checkPos(SurfacePanel level, Point pos) {
		Obstacle obs;
		Crossable cro;
		for (int i = 0; i<level.obstacles.size();i++) {
			cro = level.obstacles.get(i);
			if (!cro.isAlive()) {
				obs = (Obstacle) cro;
				if ((pos.x > (obs.p1.x - 10)) && (pos.x < (obs.p2.x + 10))
						&& (pos.y > (obs.p1.y - 10))
						&& (pos.y < (obs.p2.y + 10))) {
					return true;
				}
			}
		}
		return false;
	}

	protected boolean defineFromCorner(Pirate pirate, Obstacle obs) {
		int movX = Math.abs(pirate.movement.x);
		int movY = Math.abs(pirate.movement.y);
		if (pirate.position.x == obs.p1.x - 10
				&& pirate.position.y == obs.p1.y - 10) {
			// System.out.println("haut - gauche");
			if (movX > movY) {
				pirate.movement.x = pirate.getSpeed();
				pirate.position.x++;
				pirate.movement.y = 0;
				pirate.gravity = BOTTOM;
			} else if (movY > movX) {
				pirate.movement.y = pirate.getSpeed();
				pirate.position.y++;
				pirate.movement.x = 0;
				pirate.gravity = RIGHT;
			} else {
				setFromCorner(pirate, 0);
			}
			return true;
		}
		if (pirate.position.x == obs.p1.x - 10
				&& pirate.position.y == obs.p2.y + 10) {
			// System.out.println("bas - gauche");
			// System.out.println(pirate.movement);
			if (movX > movY) {

				pirate.movement.x = pirate.getSpeed();
				pirate.position.x++;
				pirate.movement.y = 0;
				pirate.gravity = TOP;
			} else if (movY > movX) {
				pirate.movement.y = -pirate.getSpeed();
				pirate.position.y--;
				pirate.movement.x = 0;
				pirate.gravity = RIGHT;
			} else {
				setFromCorner(pirate, 1);
			}
			return true;
		}
		if (pirate.position.x == obs.p2.x + 10
				&& pirate.position.y == obs.p1.y - 10) {
			// System.out.println("haut - droit");
			if (movX > movY) {
				pirate.movement.x = -pirate.getSpeed();
				pirate.position.x--;
				pirate.movement.y = 0;
				pirate.gravity = BOTTOM;
			} else if (movY > movX) {
				pirate.movement.y = -pirate.getSpeed();
				pirate.position.y--;
				pirate.movement.x = 0;
				pirate.gravity = LEFT;

			} else {
				setFromCorner(pirate, 2);
			}
			return true;
		}
		if (pirate.position.x == obs.p2.x + 10
				&& pirate.position.y == obs.p2.y + 10) {
			// System.out.println("bas - droit");
			if (movX > movY) {
				pirate.movement.x = -pirate.getSpeed();
				pirate.position.x--;
				pirate.movement.y = 0;
				pirate.gravity = TOP;
			} else if (movY > movX) {
				pirate.movement.y = -pirate.getSpeed();
				pirate.position.y--;
				pirate.movement.x = 0;
				pirate.gravity = LEFT;
			} else {
				pirate.gravity.setFromCorner(pirate, 3);
			}
			// System.out.println(pirate.movement);
			return true;
		}
		return false;
	}

	protected abstract void setFromCorner(Pirate pirate, int i);

	private static final int JUMP = 4;

	public abstract boolean checkGround(SurfacePanel level, Pirate pirate);

	public abstract void apply(Point pos);

	public abstract int getSpeed(Point pos);

	public abstract void turnAround(Point movement);

	public abstract void jump(Pirate pirate);

	public abstract int getAnimationRow(Point movement);

	public abstract void setMovementAndGravityFromObs(Pirate pirate,
			Obstacle obs);

	public abstract void speedUp(Point movement);

}