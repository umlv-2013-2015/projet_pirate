package com.application.projet_pirates;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Sprite {

	private static final int BMP_ROWS = 8;
	private static final int BMP_COLUMNS = 3;

	private Bitmap image;
	private int currentFrame;
	private int width;
	private int height;
	Rect src = new Rect();
	Rect dst = new Rect();

	public Sprite(Bitmap bmp) {
		this.width = bmp.getWidth() / BMP_COLUMNS;
		this.height = bmp.getHeight() / BMP_ROWS;
		this.image = bmp;
	}

	public void update() {
		currentFrame = (currentFrame + 1) % BMP_COLUMNS;

	}

	public void render(Canvas canvas, int size, int drawFromHeight,
			int drawFromWidth, Pirate pirate) {
		int srcX = (currentFrame * width);
		int srcY = (pirate.gravity.getAnimationRow(pirate.movement) * height);

		src.left = srcX + 1;
		src.top = srcY + 1;
		src.right = srcX + width - 1;
		src.bottom = srcY + height - 1;

		dst.left = drawFromWidth + (pirate.position.x) * size;
		dst.top = drawFromHeight + (pirate.position.y) * size;
		dst.right = drawFromWidth + (pirate.position.x + 10) * size;
		dst.bottom = drawFromHeight + (pirate.position.y + 10) * size;

		canvas.drawBitmap(image, src, dst, null);
	}
}