package com.application.projet_pirates;

import android.content.Context;
import android.content.SharedPreferences;

public class SettingsPrefs {
	public static final String PREF_FILE_NAME = "Settings";

	
	public static  boolean getSoundState(Context context){
		return context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).getBoolean( context.getString(R.string.soundState),true);
	}
	
	public static boolean isStaticJumpZone(Context context){
		return context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).getBoolean( context.getString(R.string.static_zone),true);
	}
	public static SharedPreferences getSharedPreferences(Context context){
		return context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
	}
	
	public static void setZoneType(Context context){
	//TODO
	}
	
}