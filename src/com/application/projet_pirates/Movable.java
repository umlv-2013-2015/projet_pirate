package com.application.projet_pirates;

public abstract class Movable {

	protected Point movement;

	public abstract void move();

	
}
