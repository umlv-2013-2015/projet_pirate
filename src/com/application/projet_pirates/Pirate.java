package com.application.projet_pirates;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.media.SoundPool;

public class Pirate extends Movable implements Crossable {
	public static final int DEFAULT_HEALTH = 5;
	
	SurfacePanel level;
	
	Point position;
	Gravity gravity;
	int currentHealth;
	final String name;
	Sprite sprite;
	
	boolean onJump = false;
	boolean onAir = true;
	
	Rect recMov = new Rect();
	int accel = 0;
	private int upd = 0;
	int invulnerabilityTimer = 0;

	public Pirate(SurfacePanel level, Bitmap img, String name) {
		this(level, img, name, 0, 0, 1, 0, DEFAULT_HEALTH, Gravity.BOTTOM);
	}

	/*
	 * Create a Pirate.
	 */
	public Pirate(SurfacePanel level, Bitmap img, String name, int x, int y,
			int mx, int my, int health, Gravity gravity) {
		this.level = level;
		this.gravity = gravity;
		this.position = new Point(x, y);
		this.movement = new Point(mx, my);
		sprite = new Sprite(img);
		this.name = name;
		this.currentHealth = health;
	}

	public Point getPosition() {
		return position;
	}

	public void setPosition(int f, int g) {
		position.x = f;
		position.y = g;
	}

	@Override
	public void draw(Canvas canvas, int size, int drawFromHeight,
			int drawFromwidth) {
		// TODO
		// update and draw the sprite
		if (invulnerabilityTimer < 1 || invulnerabilityTimer % 2 == 0) {
			if (gravity.getSpeed(movement) > 3)
				upd = 0;
			if (upd % 2 == 0)
				this.sprite.update();
			this.sprite.render(canvas, size, drawFromHeight, drawFromwidth,
					this);
			upd++;
		}
	};

	@Override
	public boolean isAlive() {
		return true;
	}

	public boolean fight(Pirate enemy) {
		if (this.invulnerabilityTimer>0|| enemy.invulnerabilityTimer>0)
			return false;
		int outCome = this.getSpeed() - enemy.getSpeed();
		
		if (outCome < 0) {//enemy win

			System.out.println("fight between " + this.name + " and "
					+ enemy.name);
			if (this.currentHealth == 1) {
				level.end(enemy);
			}
			this.currentHealth--;
			//TODO actualiser HP.
			this.invulnerabilityTimer = 20;
			System.out.println(enemy.name + " won! " + this.name
					+ "'s health = " + this.currentHealth);
			return false;

		}
		if (outCome > 0) {//I win

			System.out.println("fight between " + this.name + " and "
					+ enemy.name);
			if (enemy.currentHealth == 1) {
				level.end(this);
			}
			enemy.currentHealth--;
			//TODO actualiser HP.
			enemy.invulnerabilityTimer = 20;
			System.out.println(this.name + " won! " + enemy.name
					+ "'s health = " + enemy.currentHealth);
		}
		return true;
	}

	public int getSpeed() {
		return gravity.getSpeed(movement);
	}

	public void move() {
		invulnerabilityTimer--;
		if (onJump)
			onAir = true;
		else
			onAir = !gravity.checkGround(level, this);

		if (!onAir)
			accel--;

		List<Crossable> obstaclesCrossed = checkCollision();
		if (obstaclesCrossed.isEmpty()) {
			goForward();
		} else {
			goToObstacle(obstaclesCrossed);
		}
		onJump = false;
	}

	private void goToObstacle(List<Crossable> obstaclesCrossed) {
		int nbMove;
		int movx = Math.abs(movement.x);
		int movy = Math.abs(movement.y);
		if (movx == 0)
			nbMove = movy;
		else if (movy == 0)
			nbMove = movx;
		else
			nbMove = movx > movy ? movx / movy : movy / movx;

		// Log.v("nbMove", nbMove + "");
		int tmpX = position.x;
		int tmpY = position.y;
		Obstacle obsCrossed;
		if (movement.x < 0)
			tmpX -= 1;
		else if (movement.x > 0)
			tmpX += 1;
		if (movement.y < 0)
			tmpY -= 1;
		else if (movement.y > 0)
			tmpY += 1;
		for (int i = 0; i < nbMove; ++i) {
			if ((obsCrossed = getTheObstacleCrossed(tmpX, tmpY,
					obstaclesCrossed)) != null) {
				if (onAir) {
					setMovementAndGravityFromCollision(obsCrossed);
				} else {
					turnAround();
				}
				return;
			}

			setPosition(tmpX, tmpY);
			tmpX += movement.x / nbMove;
			tmpY += movement.y / nbMove;
		}
	}

	private void turnAround() {
		this.gravity.turnAround(movement);

	}

	private void setMovementAndGravityFromCollision(Obstacle obs) {
		gravity.setMovementAndGravityFromObs(this, obs);
	}

	private Obstacle getTheObstacleCrossed(int posx, int posy,
			List<Crossable> obstaclesCrossed) {
		Obstacle obs;
		Crossable cro;
		for (int i = 0; i < obstaclesCrossed.size(); i++) {
			cro = obstaclesCrossed.get(i);
			if (!cro.isAlive()) {
				obs = (Obstacle) cro;
				if (posx > (obs.p1.x - 10) && posx < (obs.p2.x + 10)
						&& posy > (obs.p1.y - 10) && posy < (obs.p2.y + 10))
					return ((Obstacle) obs);
			} else {
				// TODO
				if (!fight((Pirate) cro))
					level.updateHP((Pirate)cro);
				else
					level.updateHP(this);
			}
		}
		return null;
	}

	private void goForward() {
		this.setPosition(position.x + movement.x, position.y + movement.y);
	}

	public List<Crossable> checkCollision() {
		// TODO maybe a LinkedList to add the pirate crossed at start.
		ArrayList<Crossable> obstaclesCrossed = new ArrayList<Crossable>();
		Obstacle obs;
		Pirate pir;
		Crossable cro;
		for (int i = 0; i < level.obstacles.size(); i++) {
			cro = level.obstacles.get(i);
			if (movement.x > 0) {
				if (movement.y > 0) {
					recMov.left = position.x;
					recMov.top = position.y;
					recMov.right = movement.x + position.x;
					recMov.bottom = movement.y + position.y;
				} else {
					recMov.left = position.x;
					recMov.top = position.y + movement.y;
					recMov.right = movement.x + position.x;
					recMov.bottom = position.y;
				}
			} else {
				if (movement.y > 0) {
					recMov.left = position.x + movement.x;
					recMov.top = position.y;
					recMov.right = position.x;
					recMov.bottom = position.y + movement.y;
				} else {
					recMov.left = position.x + movement.x;
					recMov.top = position.y + movement.y;
					recMov.right = position.x;
					recMov.bottom = position.y;
				}
			}
			if (!cro.isAlive()) {
				obs = (Obstacle) cro;
				if (Rect.intersects(recMov, new Rect(obs.p1.x - 10,
						obs.p1.y - 10, obs.p2.x + 10, obs.p2.y + 10))) {

					obstaclesCrossed.add(cro);
				}
			} else {
				if (invulnerabilityTimer < 1) {
					pir = (Pirate) cro;
					if (!pir.equals(this)) {
						if (pir.invulnerabilityTimer < 1) {
							if (Rect.intersects(recMov, new Rect(
									pir.position.x - 10, pir.position.y - 10,
									pir.position.x + 10, pir.position.y + 10))) {
								obstaclesCrossed.add(cro);
							}
						}
					}
				}

			}
		}
		return obstaclesCrossed;
	}

	public void jump() {
		onJump = true;
		gravity.jump(this);
	}

	public void reactToTouch(SoundPool sp, int[] sounds) {
		if (accel > 0) {
			speedUp();
			sp.play(sounds[1], 1, 1, 0, 0, 1);// ringtone when click
			return;
		}
		if (!onAir) {
			sp.play(sounds[0], 1, 1, 0, 0, 1);// ringtone when click
			jump();
		}
	}

	private void speedUp() {
		gravity.speedUp(movement);
		accel = 0;
	}

}
