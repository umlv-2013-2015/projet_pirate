package com.application.projet_pirates;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

public class SurfacePanel extends SurfaceView implements SurfaceHolder.Callback {
	// public static final double TRANSPARENCY = 0.5;
	private int move = 0;
	char[][] lvltxt;
	List<Crossable> obstacles = new ArrayList<Crossable>();
	List<Movable> movables = new ArrayList<Movable>();
	List<JumpZone> zones = new ArrayList<JumpZone>();

	private int drawFromHeight = 0;
	private int drawFromWidth = 0;
	public int size;// taille d'une case(pixel)

	Activity myActivity;

	private Context context;

	public Thread drawthread;
	public Object mRunLock = new Object();
	boolean running;

	private MediaPlayer gameMusic;
	SoundPool sp;
	int sounds[];

	public SurfacePanel(Context ctx, AttributeSet attrSet) {
		super(ctx, attrSet);

		context = ctx;
		getHolder().addCallback(this);

		sounds = new int[10];

	}

	public void setActivity(Activity act) {
		myActivity = act;
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.w("GameActivity", "beginning of surfaceCreated");

		startThread();
		if (!drawthread.isAlive()) {
			drawthread.start();
		}

		if (SettingsPrefs.getSoundState(getContext())) {
			// music cr��e ici car sinon NullPointerException
			gameMusic = MediaPlayer.create(getContext(), R.raw.game_music);
			gameMusic.setLooping(true);
			gameMusic.setAudioStreamType(AudioManager.STREAM_MUSIC);
			gameMusic.start();
		}

		// prepare Audio file
		sp = new SoundPool(2, AudioManager.STREAM_MUSIC, 0);
		sounds[0] = sp.load(context, R.raw.jump, 1);
		sounds[1] = sp.load(context, R.raw.cri_de_guerre, 1);

		loadGame();
	}

	public void startThread() {
		drawthread = new Thread(new Runnable() {

			private Canvas mcanvas;

			@Override
			public void run() {

				while (running) { // When setRunning(false) occurs, _run is
					mcanvas = null; // set to false and loop ends, stopping
									// thread
					try {
						try {
							Thread.sleep(30);
						} catch (InterruptedException e) {
							running = false;
							Thread.currentThread().interrupt();
						}
						mcanvas = getHolder().lockCanvas(null);
						synchronized (getHolder()) {
							doDraw(mcanvas);
							postInvalidate();
							doMove();
						}
					} finally {
						if (mcanvas != null) {
							getHolder().unlockCanvasAndPost(mcanvas);
						}
					}
				}
			}

		});

	}

	public void stopThread() {
		System.out.println("1");
		synchronized (mRunLock) {
			System.out.println("2");

			while (running) {
				try {
					running = false;
					drawthread.interrupt();
					drawthread = null;
					System.out.println("wait");
				} catch (Exception e) {
					running = false;
					Thread.currentThread().interrupt();
				}
			}

		}

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		running = false;
		stopThread();
	}

	public void doDraw(Canvas canvas) {
		canvas.drawColor(Color.WHITE);
		for (int i = 0; i < obstacles.size(); i++) {
			obstacles.get(i).draw(canvas, size, drawFromHeight, drawFromWidth);
		}

		for (int i = 0; i < zones.size(); i++) {
			zones.get(i).draw(canvas);
		}
		move++;
	}

	public void doMove() {
		for (int i = 0; i < movables.size(); i++) {
			movables.get(i).move();
		}

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		// get pointer index from the event object
		int pointerIndex = event.getActionIndex();
		// get masked (not specific to a pointer) action
		int maskedAction = event.getActionMasked();

		switch (maskedAction) {
		case MotionEvent.ACTION_POINTER_DOWN:
		case MotionEvent.ACTION_DOWN:
			int touchX = (int) event.getX(pointerIndex);
			int touchY = (int) event.getY(pointerIndex);
			JumpZone zone;
			for (int i = 0; i < zones.size(); i++) {
				zone = zones.get(i);
				if (zone.jumpRect.contains(touchX, touchY)) {
					zone.pirate.reactToTouch(sp, sounds);
				}
			}
			break;
		}

		return true;
	}

	private char[][] readFile(String fileName) throws IOException {
		char[][] content;
		int cpt = 0;
		int length = 0;
		LinkedList<Character> contentLine = new LinkedList<Character>();
		String current;
		Scanner scan = null;
		try {
			scan = new Scanner(context.getAssets().open(fileName));
			while (scan.hasNextLine()) {
				current = scan.nextLine();
				cpt++;
				if ((length == 0) && (current.length() > length))
					length = current.length();
				if (current.length() != length)
					return null; // ERROR
				for (int i = 0; i < length; i++) {
					contentLine.addLast(current.charAt(i));
				}
			}
		} finally {
			scan.close();
		}
		content = new char[cpt][];
		for (int i = 0; i < cpt; i++) {
			content[i] = new char[length];
			for (int j = 0; j < length; j++) {
				content[i][j] = contentLine.poll();
			}
		}
		return content;
	}

	public void loadLevel(int numLevel) throws IOException {
		lvltxt = readFile("lvl" + numLevel);
	}

	public void loadGame() {
		Pirate pirate = null;
		Obstacle obs = null;
		boolean rotation = false;
		int x1, y1, x2, y2, tmpi;
		int gameWidth = this.getWidth();
		int gameHeight = this.getHeight();
		System.out.println(lvltxt);
		if (lvltxt == null)
			return;
		int maxHeight = lvltxt.length;
		int maxWidth = lvltxt[0].length;
		System.out.println("height " + lvltxt[0].length);
		System.out.println("width " + lvltxt.length);

		if ((gameHeight / (10 * maxHeight)) < (gameWidth / (10 * maxWidth))) {
			rotation = false;
			size = (gameHeight / (10 * maxHeight));
		} else {
			rotation = true;
			size = (gameWidth / (10 * maxWidth));
		}
		drawFromHeight = (gameHeight - (size * (10 * maxHeight))) / 2;
		drawFromWidth = (gameWidth - (size * (10 * maxWidth))) / 2;
		for (int i = 0; i < lvltxt.length; i++) {
			for (int j = 0; j < lvltxt[i].length; j++) {
				switch (lvltxt[i][j]) {
				case 'x':
					x2 = x1 = i;
					y2 = y1 = j;
					lvltxt[i][j] = ' ';
					if ((j + 1 < lvltxt[i].length) && (lvltxt[i][j + 1] == 'x')) {
						j++;
						while ((j < lvltxt[i].length) && (lvltxt[i][j] == 'x')) {
							lvltxt[i][j] = ' ';
							y2 = j;
							j++;
						}
					} else {
						tmpi = i;
						if ((tmpi + 1 < lvltxt.length)
								&& (lvltxt[tmpi + 1][j] == 'x')) {
							tmpi++;
							while ((tmpi < lvltxt.length)
									&& (lvltxt[tmpi][j] == 'x')) {
								lvltxt[tmpi][j] = ' ';
								x2 = tmpi;
								tmpi++;
							}
						}
					}
					if (!rotation)
						obs = new Obstacle(this, x1 * 10, y1 * 10, x2 * 10,
								y2 * 10);
					else
						obs = new Obstacle(this, y1 * 10, x1 * 10, y2 * 10,
								x2 * 10);
					System.out.println(obs);
					obstacles.add(obs);
					break;
				case '1':
					if (!rotation)
						pirate = new Pirate(this, BitmapFactory.decodeResource(
								context.getResources(), R.drawable.testgreen),
								"greeny", i * 10, j * 10, 0, 0,
								Pirate.DEFAULT_HEALTH, Gravity.LEFT);
					else
						pirate = new Pirate(this, BitmapFactory.decodeResource(
								context.getResources(), R.drawable.testgreen),
								"greeny", j * 10, i * 10, 0, 0,
								Pirate.DEFAULT_HEALTH, Gravity.LEFT);
					obstacles.add(pirate);
					movables.add(pirate);

					JumpZone jumpGreen;
					if (SettingsPrefs.isStaticJumpZone(getContext())) {
						jumpGreen = new JumpZone(pirate, 0.8, Color.GREEN,
								new Point(0, 0), gameWidth/4, gameHeight, 0,
								0, false);
					} else {
						jumpGreen = new JumpZone(pirate, 0.8, Color.GREEN,
								pirate.position, size, size, drawFromHeight,
								drawFromWidth, true);

					}
					zones.add(jumpGreen);
					myActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
								((TextView) myActivity.findViewById(R.id.lifePoint1))
										.setText("Greeny HP:" +Pirate.DEFAULT_HEALTH);
						}
					});
					break;
				case '2':
					if (!rotation)
						pirate = new Pirate(this, BitmapFactory.decodeResource(
								context.getResources(), R.drawable.testred),
								"redy", i * 10, j * 10, 0, 0,
								Pirate.DEFAULT_HEALTH, Gravity.LEFT);
					else
						pirate = new Pirate(this, BitmapFactory.decodeResource(
								context.getResources(), R.drawable.testred),
								"redy", j * 10, i * 10, 0, 0,
								Pirate.DEFAULT_HEALTH, Gravity.LEFT);
					obstacles.add(pirate);
					movables.add(pirate);
					JumpZone jumpRed;
					if (SettingsPrefs.isStaticJumpZone(getContext())) {
						jumpRed = new JumpZone(pirate, 0.8, Color.RED,
								new Point(3 * gameWidth / 4, 0), gameWidth/4,
								gameHeight, 0, 0, false);
					} else {
						jumpRed = new JumpZone(pirate, 0.8, Color.RED,
								pirate.position, size, size, drawFromHeight,
								drawFromWidth, true);

					}
					zones.add(jumpRed);
					myActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
								((TextView) myActivity.findViewById(R.id.lifePoint2))
										.setText("Redy HP:" +Pirate.DEFAULT_HEALTH);
						}
					});
					break;
				case '3':
					break;
				case '4':
					break;
				case ' ':
					break;
				}
			}
		}
		for (int i = 0; i < movables.size(); i++) {
			pirate = (Pirate) movables.get(i);
			if (pirate.gravity.checkGround(this, pirate)) {
				pirate.movement.y = 1;
				continue;
			}

			pirate.gravity = Gravity.TOP;
			if (pirate.gravity.checkGround(this, pirate)) {
				pirate.movement.x = -1;
				continue;
			}
			pirate.gravity = Gravity.RIGHT;
			if (pirate.gravity.checkGround(this, pirate)) {
				pirate.movement.y = -1;
				continue;
			}

			pirate.gravity = Gravity.BOTTOM;
			if (pirate.gravity.checkGround(this, pirate)) {
				pirate.movement.x = 1;
				continue;
			}
		}
		updateHP(pirate);
		
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
	}

	public void end(Pirate pirate)  {
		System.out.println(pirate.name + " won the game!");
		final Pirate pirateF = pirate;
		myActivity.runOnUiThread(new Runnable() {

		    @Override
		    public void run() {
		    	((TextView) myActivity.findViewById(R.id.win))
				.setText(pirateF.name+ " WINS!");

		    }
		    });
		zones.clear();
		//myActivity.finish();
	}

	public void updateHP(Pirate pirate) {

		final Pirate pirateF = pirate;
		myActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {

				if (pirateF.name == "greeny") {
					((TextView) myActivity.findViewById(R.id.lifePoint1))
							.setText("Greeny HP:" + pirateF.currentHealth);
				} else {
					((TextView) myActivity.findViewById(R.id.lifePoint2))
							.setText("Redy HP:" + pirateF.currentHealth);
				}
			}
		});
	}

	public void pause() {
		if (SettingsPrefs.getSoundState(context)) {
			gameMusic.pause();
		}
	}

	public void resume() {
		if (SettingsPrefs.getSoundState(context)) {
			gameMusic.start();
		}
	}
}