package com.application.projet_pirates;

import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class GameActivity extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.w("GameActivity", "beginning onCreate");
		super.onCreate(savedInstanceState);
		
		if (savedInstanceState != null)
			return;
		// remove title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.game_activity);
		// retrieve the SurfacePanel of the xml Layout
		SurfacePanel spanel = (SurfacePanel) findViewById(R.id.surface);
		try {
			// the argument given in loadLevel() is the level number. Should be
			// given by the user.
			spanel.loadLevel(getIntent().getIntExtra("level", 1));
			spanel.running = true;
			spanel.setActivity(this);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onPause() {
		super.onPause();
		SurfacePanel spanel = (SurfacePanel) findViewById(R.id.surface);
		spanel.pause();//stop music
		spanel.stopThread();
		//spanel.running = false;

	}

	@Override
	protected void onResume() {
		super.onResume();
		SurfacePanel spanel = (SurfacePanel) findViewById(R.id.surface);
		synchronized (spanel.mRunLock) {
			spanel.running = true;
			spanel.mRunLock.notifyAll();
			System.out.println("notified");
			;
		}
		//spanel.resume();
	}
}