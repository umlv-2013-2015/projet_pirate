package com.application.projet_pirates;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class JumpZone {
	public static final double TRANSPARENCY = 0.6;
	private static final int BIG = 30;
	public Rect jumpRect;// un rectangle cliquable
	Paint semitransparentPaint;
	protected Pirate pirate;// Un pirate associ� , pour faire le saut

	public boolean isDynamic;
	private int sizex, sizey;
	private static int drawFromWidth;
	private static int drawFromHeight;

	public JumpZone(Pirate pirate, double alpha, int rectColor, Point position,
			int sizex, int sizey, int drawFromHeight, int drawFromWidth,
			boolean isDynamic) {
		this.pirate = pirate;
		this.semitransparentPaint = new Paint();
		semitransparentPaint.setColor(rectColor);
		semitransparentPaint.setAlpha((int) (256 * (1 - alpha)));
		JumpZone.drawFromHeight=drawFromHeight;
		JumpZone.drawFromWidth=drawFromWidth;
		this.isDynamic = isDynamic;
		if (isDynamic){
			jumpRect = new Rect((position.x * sizex) - BIG + drawFromWidth,
					(position.y * sizey) - BIG + drawFromHeight,
					((position.x + 10) * sizex) + BIG + drawFromWidth,
					((position.y + 10) * sizey) + BIG + drawFromHeight);
		}
		else
			jumpRect = new Rect(position.x, position.y, position.x + sizex,
					position.y + sizey);
		this.sizex = sizex;
		this.sizey = sizey;
	}

	public JumpZone(Pirate pirate, Point position, int drawFromHeight,
			int drawFromWidth) {
		this(pirate, TRANSPARENCY, Color.BLACK, position, 200, 200,
				drawFromHeight, drawFromWidth, true);
	}

	public void updatePosition() {
		jumpRect.offsetTo((pirate.position.x * sizex) - BIG +JumpZone.drawFromWidth,
				(pirate.position.y * sizey) - BIG + JumpZone.drawFromHeight);
	}

	public void draw(Canvas canvas) {
		if (isDynamic)
			updatePosition();
		else
			canvas.drawRect(jumpRect, semitransparentPaint);
		// canvas.drawLine((float)jumpRect.left,(float)jumpRect.top,(float)jumpRect.right,(float)jumpRect.bottom,
		// semitransparentPaint);
	};

}
