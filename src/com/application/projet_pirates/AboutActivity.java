package com.application.projet_pirates;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class AboutActivity extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// remove title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// on lui associe le layout
		setContentView(R.layout.about_activity);

		TextView aboutInfo = (TextView) findViewById(R.id.aboutInfo);
		Typeface font = Typeface.createFromAsset(getAssets(), "jacinto.otf");
		aboutInfo.setTypeface(font);
	}

	@Override
	public void finish() {
	    super.finish();
	    overridePendingTransition(0, 0);
	}  
}