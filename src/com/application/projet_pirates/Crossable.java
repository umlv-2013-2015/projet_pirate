package com.application.projet_pirates;

public interface Crossable extends Drawable {
	public boolean isAlive();
}
