package com.application.projet_pirates;

public class Point {
	int x;
	int y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Point add(Point pos) {
		return new Point(this.x+pos.x,this.y+pos.y);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("(");
		builder.append(x);
		builder.append(";");
		builder.append(y);
		builder.append(")");
		return builder.toString();
	}
	
}
